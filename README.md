# BroadcastReceiver Sample App

## Project Configuration

You will need to provide your own openstates.org API key for the app to work. Open or create the gradle.properties file located in .gradle directory
on your local machine and add the following line:

OpenStatesAPIKey=YOUR_API_KEY

where you replace YOUR_API_KEY with your own key.

## Libraries

* [ButterKnife](https://github.com/JakeWharton/butterknife)
* [Retrofit](https://github.com/square/retrofit)
