package com.kouroshraeen.broadcastreceiversampleapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class MyBroadcastReceiver extends BroadcastReceiver {
    private Receiver mReceiver;

    @Override
    public void onReceive(Context context, Intent intent) {
        mReceiver.onReceive(context, intent);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    public interface Receiver {
        public void onReceive(Context context, Intent intent);
    }
}
