package com.kouroshraeen.broadcastreceiversampleapp.api;


import com.kouroshraeen.broadcastreceiversampleapp.model.Bill;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenStatesApi {
    String OPEN_STATES_BASE_URL = "http://openstates.org/api/v1/";

    @GET("bills/")
    Call<List<Bill>> getBills(@Query("state") String state, @Query("search_window") String search_window, @Query("page") String page, @Query("sort") String sort, @Query("apikey") String api_key);

}
