package com.kouroshraeen.broadcastreceiversampleapp.api;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static OpenStatesApi apiService;

    public static OpenStatesApi getApiClient(){
        // Check to see if an OpenStatesApi is already instantiated
        if(apiService == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(OpenStatesApi.OPEN_STATES_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiService = retrofit.create(OpenStatesApi.class);
        }
        return apiService;
    }
}

