package com.kouroshraeen.broadcastreceiversampleapp.service;


import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.kouroshraeen.broadcastreceiversampleapp.BuildConfig;
import com.kouroshraeen.broadcastreceiversampleapp.api.ApiClient;
import com.kouroshraeen.broadcastreceiversampleapp.api.OpenStatesApi;
import com.kouroshraeen.broadcastreceiversampleapp.model.Bill;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class QueryService extends IntentService {
    private static final String TAG = QueryService.class.getSimpleName();
    public static final String ACTION = "com.kouroshraeen.broadcastreceiversampleapp.service.QueryService";
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public QueryService() {
        super("QueryService");
    }

    public QueryService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        OpenStatesApi apiService = ApiClient.getApiClient();
        Call<List<Bill>> call = apiService.getBills("ks", "session", "", "created_at", BuildConfig.OPEN_STATES_API_KEY);
        String url = call.request().url().toString();
        Log.i(TAG, url);

        try {
            ArrayList<Bill> bills = (ArrayList<Bill>) call.execute().body();
            //Log.i(TAG, "Number of bills: " + bills.size());
            Intent broadcastIntent = new Intent(ACTION);
            broadcastIntent.putExtra("resultCode", Activity.RESULT_OK);
            broadcastIntent.putParcelableArrayListExtra("bills", bills);
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        } catch (IOException e) {

        }
    }
}

