package com.kouroshraeen.broadcastreceiversampleapp.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Bill implements Parcelable {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("chamber")
    @Expose
    private String chamber;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("session")
    @Expose
    private String session;
    @SerializedName("type")
    @Expose
    private List<String> type = new ArrayList<String>();
    @SerializedName("subjects")
    @Expose
    private List<Object> subjects = new ArrayList<Object>();
    @SerializedName("bill_id")
    @Expose
    private String billId;

    protected Bill(Parcel in) {
        title = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        id = in.readString();
        chamber = in.readString();
        state = in.readString();
        session = in.readString();
        type = in.createStringArrayList();
        billId = in.readString();
    }

    public static final Creator<Bill> CREATOR = new Creator<Bill>() {
        @Override
        public Bill createFromParcel(Parcel in) {
            return new Bill(in);
        }

        @Override
        public Bill[] newArray(int size) {
            return new Bill[size];
        }
    };

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The chamber
     */
    public String getChamber() {
        return chamber;
    }

    /**
     * @param chamber The chamber
     */
    public void setChamber(String chamber) {
        this.chamber = chamber;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The session
     */
    public String getSession() {
        return session;
    }

    /**
     * @param session The session
     */
    public void setSession(String session) {
        this.session = session;
    }

    /**
     * @return The type
     */
    public List<String> getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(List<String> type) {
        this.type = type;
    }

    /**
     * @return The subjects
     */
    public List<Object> getSubjects() {
        return subjects;
    }

    /**
     * @param subjects The subjects
     */
    public void setSubjects(List<Object> subjects) {
        this.subjects = subjects;
    }

    /**
     * @return The billId
     */
    public String getBillId() {
        return billId;
    }

    /**
     * @param billId The bill_id
     */
    public void setBillId(String billId) {
        this.billId = billId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        parcel.writeString(id);
        parcel.writeString(chamber);
        parcel.writeString(state);
        parcel.writeString(session);
        parcel.writeStringList(type);
        parcel.writeString(billId);
    }
}

