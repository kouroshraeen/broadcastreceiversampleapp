package com.kouroshraeen.broadcastreceiversampleapp;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.kouroshraeen.broadcastreceiversampleapp.adapter.BillsAdapter;
import com.kouroshraeen.broadcastreceiversampleapp.model.Bill;
import com.kouroshraeen.broadcastreceiversampleapp.service.QueryService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MyBroadcastReceiver.Receiver {
    public static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Bill> mBills;
    private BillsAdapter mAdapter;
    private MyBroadcastReceiver mReceiver;

    @BindView(R.id.bill_list)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Bills in Kansas state legislature");
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        mReceiver = new MyBroadcastReceiver();
        mReceiver.setReceiver(this);

        Intent intent = new Intent(this, QueryService.class);
        startService(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(QueryService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int resultCode = intent.getIntExtra("resultCode", RESULT_CANCELED);
        if (resultCode == RESULT_OK) {
            mBills = intent.getParcelableArrayListExtra("bills");
            updateUi();
        }
    }

    private void updateUi() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider));
        mAdapter = new BillsAdapter(this, mBills);
        mRecyclerView.setAdapter(mAdapter);
    }
}
