package com.kouroshraeen.broadcastreceiversampleapp.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kouroshraeen.broadcastreceiversampleapp.R;
import com.kouroshraeen.broadcastreceiversampleapp.model.Bill;
import com.kouroshraeen.broadcastreceiversampleapp.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BillsAdapter extends RecyclerView.Adapter {

    public List<Bill> mBills;
    private static Context mContext;
    private boolean mEmpty = false;


    public BillsAdapter(Context context, List<Bill> bills) {
        mBills = bills;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View billView = inflater.inflate(R.layout.bill_item, parent, false);
        return new BillViewHolder(billView);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Bill bill = mBills.get(position);

        TextView billDateTextView = ((BillViewHolder) holder).billDateTextView;

        billDateTextView.setText(Utils.getFormattedDateString(bill.getCreatedAt()));

        TextView billIdTextView = ((BillViewHolder) holder).billIdTextView;
        billIdTextView.setText(bill.getBillId());

        TextView billTitleTextView = ((BillViewHolder) holder).billTitleTextView;
        billTitleTextView.setText(bill.getTitle());

    }

    @Override
    public int getItemCount() {
        if (mBills == null) {
            return 0;
        } else {
            return mBills.size() + 1;
        }
    }

    public void setData(List<Bill> data) {
        if (null != data && !data.isEmpty()) {
            if (mBills == null) {
                mBills = data;
            } else {
                mBills.addAll(data);
            }
            notifyDataSetChanged();
        }
    }

    public static class BillViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.bill_item)
        LinearLayout billItemLinearLayout;
        @BindView(R.id.bill_date)
        TextView billDateTextView;
        @BindView(R.id.bill_id)
        TextView billIdTextView;
        @BindView(R.id.bill_title)
        TextView billTitleTextView;

        public BillViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }
}

