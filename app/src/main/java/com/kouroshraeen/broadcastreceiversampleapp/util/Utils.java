package com.kouroshraeen.broadcastreceiversampleapp.util;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public static String getFormattedDateString(String dateString) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDateString = "";
        try {
            Date date = format.parse(dateString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            String month = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            String dayString = String.valueOf(day);
            if (dayString.length() == 1) {
                dayString = "0" + dayString;
            }
            int year = calendar.get(Calendar.YEAR);
            String yearString = String.valueOf(year);
            yearString = yearString.substring(2, 4);
            formattedDateString = month + " " + dayString + ", '" + yearString;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedDateString;
    }
}

